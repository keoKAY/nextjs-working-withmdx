/** @type {import('next').NextConfig} */
const nextConfig = {
  async redirects() {
    return [
      {
        source: "/documentation",
        destination: "/documentation/introduction",
        permanent: true, // Set to false if the redirect is temporary
      },
      {
        source: "/documentation/deployment",
        destination: "/documentation/deployment/frontend",
        permanent: true, // Set to false if the redirect is temporary
      },
    ];
  },
};

module.exports = nextConfig;
