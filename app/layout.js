import { Inter } from "next/font/google";
import "./globals.css";

//const inter = Inter({ subsets: ["latin"] });
import FlowbiteNavbar from "../components/FlowbiteNavbar";
export const metadata = {
  title: "Blogs website from scratch!",
  description: "Created by a very handsome man.",
};

export default function RootLayout({ children }) {
  return (
    <html lang="en" className="bg-slate-900 text-white ">
      <body>
        <FlowbiteNavbar />
        <div style={{ width: "100%" }}>{children}</div>
      </body>
    </html>
  );
}
