import fs from "fs";
import path from "path";
import matter from "gray-matter";
import { MDXRemote } from "next-mdx-remote/rsc";
import CodeBlock from "../../../components/CodeBlock";
export default function Page({ params }) {
  const { slug } = params;
  console.log("Slug is : ", slug);
  const markdownFile = fs.readFileSync(
    path.join("documentation", slug + ".mdx"),
    "utf-8",
  );
  const { data: frontMatter, content } = matter(markdownFile);

  const components = {
    code: CodeBlock,
  };
  return (
    <article className="prose prose-sm md:prose-base lg:prose-lg prose-slate !prose-invert mt-5 mx-auto">
      <h1>{frontMatter.title}</h1>
      <MDXRemote source={content} components={components}></MDXRemote>
    </article>
  );
}
