import Link from "next/link";
import fs from "fs";
import path from "path";
import matter from "gray-matter";
export default function Page() {
  const blogsDirectory = "documentation";
  const files = fs.readdirSync(blogsDirectory);

  const blogs = files.map((filename) => {
    const fileContent = fs.readFileSync(
      path.join(blogsDirectory, filename),
      "utf8",
    );
    const { data } = matter(fileContent);

    return {
      meta: data,
      slug: filename.replace(".mdx", ""),
    };
  });

  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <h1> This is all my blogs </h1>

      <div className="py-2  text-black  m-2 rounded">
        {blogs.map((blog) => (
          <div key={blog.slug} className="py-2 px-2 mt-2 bg-white">
            <Link href={`/documentation/${blog.slug}`}>
              <p className="text-2xl font-bold text-blue-400">
                {blog.meta.title}
              </p>
            </Link>
            <h1> {blog.meta.title} </h1>
            <p>{blog.meta.description}</p>
            <p>{blog.meta.date}</p>
          </div>
        ))}
      </div>
    </main>
  );
}
