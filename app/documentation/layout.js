"use client";
import Sidebar from "../../components/CustomSideBar";
import React, { useState } from "react";
import {
  HiArrowSmRight,
  HiChartPie,
  HiInbox,
  HiShoppingBag,
  HiTable,
  HiUser,
} from "react-icons/hi";
const Layout = ({ children }) => {
  const [availableOptions, setAvailableOptions] = useState([
    {
      title: "Introduction",
      icon: HiChartPie,
      slug: "/documentation/introduction",
      children: [],
    },
    {
      title: "Deployment",
      icon: HiShoppingBag,
      slug: "/documentation/deployment",
      children: [
        { title: "Frontend", slug: "/documentation/deployment/frontend" },
        { title: "Backend", slug: "/documentation/deployment/backend" },
        { title: "Databasse", slug: "/documentation/deployment/database" },
      ],
    },
  ]);
  return (
    <div className="flex">
      <div className="w-1/4">
        <Sidebar options={availableOptions} />
      </div>
      <div className="w-3/4">
        {" "}
        {/* Content area width */}
        {children}
      </div>
    </div>
  );
};

export default Layout;
