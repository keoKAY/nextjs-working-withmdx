"use client";
import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";

// Choose a style theme for your code blocks
import { dark } from "react-syntax-highlighter/dist/cjs/languages/prism/javascript";
const CodeBlock = ({ children, className }) => {
  const language = className
    ? className.replace(/language-/, "")
    : "javascript";
  //const style = require(`react-syntax-highlighter/dist/cjs/styles/prism/${theme}`,).default;

  return (
    <SyntaxHighlighter style={dark} language={language}>
      {children}
    </SyntaxHighlighter>
  );
};

export default CodeBlock;
