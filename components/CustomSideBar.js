"use client";

import { Sidebar } from "flowbite-react";
import {
  HiArrowSmRight,
  HiChartPie,
  HiInbox,
  HiShoppingBag,
  HiTable,
  HiUser,
} from "react-icons/hi";

function CustomSideBar({ options }) {
  return (
    <Sidebar aria-label="Sidebar with multi-level dropdown example">
      <Sidebar.Items>
        <Sidebar.ItemGroup>
          {options.map((option, index) =>
            option.children.length === 0 ? (
              <Sidebar.Item key={index} href={option.slug} icon={option.icon}>
                {" "}
                {option.title}
              </Sidebar.Item>
            ) : (
              <Sidebar.Collapse
                key={index}
                icon={HiShoppingBag}
                label={option.title}
              >
                {option.children.map((child, index) => (
                  <Sidebar.Item key={index} href={child.slug}>
                    {child.title}
                  </Sidebar.Item>
                ))}
              </Sidebar.Collapse>
            ),
          )}
        </Sidebar.ItemGroup>
      </Sidebar.Items>
    </Sidebar>
  );
}

export default CustomSideBar;
